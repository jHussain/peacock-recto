const withFonts = require('next-fonts');

module.exports = withFonts({
  trailingSlash: true,
  handleImages: ['jpeg', 'png', 'svg'],
  enableSvg: true,
  images: {
    loader: 'imgix',
    path: 'https://file.iam-hussain.site/peacock',
  },
  env: {
    API_BASE_URL: process.env.API_BASE_URL || 'https://club.iam-hussain.site/api',
    APP_URL: process.env.APP_URL || 'https://peacock.iam-hussain.site',
    APP_NAME: process.env.APP_NAME || 'Peacock',
  },
  webpack(config) {
    return config;
  },
});
