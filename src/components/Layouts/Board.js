import React from "react";

// components
import Sidebar from "../Molecule/Sidebar";
import FooterAdmin from "../Molecule/Footer/FooterAdmin";

export default function Dashboard({ children }) {
  return (
    <>
      <div className="relative md:ml-64 bg-blueGray-100 min-h-screen">
      <Sidebar />
        <div className="md:px-10 mx-auto w-full pt-8 px-2">
          {children}
          <FooterAdmin />
        </div>
      </div>
    </>
  );
}
