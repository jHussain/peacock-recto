import React from "react";

// components

import CardStats from "./Cards/CardStats";

export default function Stats({ statsData  }) {
  return (
    <>
      {/* Header */}
      <div className="relative mb-10 pb-6">
        <div className="mx-auto w-full">
          <div>
            {/* Card stats */}
            <div className="flex flex-wrap justify-center gap-y-2">
              {statsData && statsData.map((each, i) => (
                <div key={i} className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats {...each}/>
              </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
