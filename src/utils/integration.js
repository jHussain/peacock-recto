import instance from './instance';

export const getMethod = async (URL) => {
  const returnData = await instance.get(URL);
  return returnData;
};

export const postMethod = async (URL, body) => {
  const returnData = await instance.post(URL, body);
  return returnData;
};

export const putMethod = async (URL, body) => {
  const returnData = await instance.put(URL, body);
  return returnData
};

export const deleteMethod = async (URL) => {
  const returnData = await instance.delete(URL);
  return returnData
};
