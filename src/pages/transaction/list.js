import React from "react";
import BoardLayout from "../../components/Layouts/Board";

import CardTransaction from "../../components/Molecule/Cards/CardTransaction";

const Summary = () => {

  return (
    <>
      <nav className=" w-full bg-transparent md:flex-row md:flex-nowrap md:justify-start flex items-center p-4 mb-6">
        <div className="w-full mx-autp items-center flex justify-between md:flex-nowrap flex-wrap">
          <span
            className="uppercase text-black text-sm inline-block font-semibold"
            onClick={(e) => handleClick(e, "/")}
          >
            All Transaction
          </span>
        </div>
      </nav>
      <>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction title="Recent Transaction" url={`transaction?`} />
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction
              title="Recently Added"
              sortBy="createdAt"
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction
              title="Deposit"
              method={"deposit"}
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction
              title="Investment"
              method={"invest"}
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction
              title="Returns"
              method={"return_on_invest"}
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction
              title="Expenditure"
              method={"expenditure"}
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction
              title="Transfer"
              method={"transfer"}
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            <CardTransaction
              title="Withdraw"
              method={"withdraw"}
            />
          </div>
        </div>
      </>
    </>
  );
};

Summary.layout = BoardLayout;

export default Summary;
