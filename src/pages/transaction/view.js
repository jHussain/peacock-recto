import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Loader from "../../components/Molecule/Loader";
import BoardLayout from "../../components/Layouts/Board";
import { getMethod } from "../../utils/integration";
// components

const Transaction = () => {
  const router = useRouter();
  const {
    limit = 20,
    page = 1,
    method,
    eMethod,
    role,
    user,
    title,
    sortBy,
  } = router.query;

  const [tableData, setTableData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isPageLoading, setPageLoading] = useState(false);
  const [userData, setUserData] = useState({});
  const [pageNum, setPageNum] = useState(Number(page));

  const loadData = async () => {
    setPageLoading(true);
    let URL = `transaction?page=${pageNum}&limit=${limit}`;

    if (method) {
      URL = `${URL}&method=${method}`;
    }
    if (eMethod) {
      URL = `${URL}&eMethod=${eMethod}`;
    }
    if (role) {
      URL = `${URL}&role=${role}`;
    }
    if (sortBy) {
      URL = `${URL}&sortBy=${sortBy}`;
    }
    if (user) {
      URL = `${URL}&user=${user}`;
      const getUser = await getMethod(`user/only/${user}`);
      if (getUser.success) {
        setUserData(getUser.data);
      } else {
        router.push("/");
      }
    }

    const { success, data } = await getMethod(URL);
    if (success) {
      setTableData(data.content);
      setLoading(false);
      setPageLoading(false);
    } else {
      router.push("/");
    }
  };

  useEffect(() => {
    loadData();
  }, [limit, page, method, eMethod, role, user, pageNum]);

  const handleClick = (e, href) => {
    e.preventDefault();
    router.push(href);
  };

  const handleDeleteClick = (e, transactionId) => {
    if(e.detail === 2) {
      router.push(`/add?transactionId=${transactionId}`);
    }
  };

  return (
    <>
      {isLoading ? (
        <Loader path="Transactions" />
      ) : (
        <>
          <nav className=" w-full bg-transparent md:flex-row md:flex-nowrap md:justify-start flex items-center p-4 mb-6">
            <div className="w-full mx-autp items-center flex justify-between md:flex-nowrap flex-wrap">
              {/* Brand */}
              {user && userData && userData.name ? (
                <a
                  className="cursor-pointer uppercase text-black text-sm uppercase inline-block font-semibold"
                  onClick={(e) => handleClick(e, `/detail/${userData.value}`)}
                >
                  User Transaction View {" / "}
                  {userData.name} {title ? ` /  ${title}` : ""}
                </a>
              ) : (
                <a
                  className="cursor-pointer uppercase text-black text-sm uppercase inline-block font-semibold"
                  onClick={(e) => handleClick(e, "/transaction/list")}
                >
                  Transaction View {" / "} {title ? title : ""}
                </a>
              )}
            </div>
          </nav>
          {tableData && tableData.length ? (
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full shadow-lg rounded">
              <div className="rounded-t mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <h3 className="font-semibold text-base text-blueGray-700 uppercase">
                      {method ? `${method}` : ""} Transactions
                    </h3>
                  </div>
                  {tableData &&
                  tableData.length > 0 &&
                  !(
                    Number(limit) > tableData.length && Number(pageNum) === 1
                  ) ? (
                    <>
                      <div className="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
                        <button
                          className={`bg-indigo-500 text-white disabled:bg-slate-50 hover:bg-indigo-800 active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none m-1 ease-linear transition-all duration-150 ${
                            isPageLoading || !(Number(pageNum) > 1) ? "opacity-20" : "opacity-100"
                          }`}
                          type="button"
                          disabled={isPageLoading}
                          onClick={() => {
                            if (Number(pageNum) > 1) {
                              setPageNum(Number(pageNum) - 1);
                            }
                          }}
                        >
                          <i className="fas fa-angle-left text-blueGray-300 px-2 text-sm"></i>
                        </button>
                        <span className="px-2">{pageNum}</span>
                        <button
                          className={`bg-indigo-500 text-white disabled:bg-slate-50 hover:bg-indigo-800 active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none m-1 ease-linear transition-all duration-150 ${
                            isPageLoading || !(tableData.length === Number(limit)) ? "opacity-20" : "opacity-100"
                          }`}
                          type="button"
                          disabled={isPageLoading}
                          onClick={() => {
                            if (tableData.length === Number(limit)) {
                              setPageNum(Number(pageNum) + 1);
                            }
                          }}
                        >
                          <i className="fas fa-angle-right text-blueGray-300 px-2 text-sm"></i>
                        </button>
                      </div>
                    </>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
              <div className="block w-full overflow-x-auto">
                {/* Projects table */}
                <table className="items-center w-full bg-transparent border-collapse">
                  <thead className="thead-light">
                    <tr>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Transactions ID
                      </th>

                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        From
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Amount
                      </th>
                      <th className=" px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Method
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        To
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        Date
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-center">
                        Delete
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {tableData && tableData.length > 0 ? (
                      tableData.map((item, i) => (
                        <tr key={i}>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item._id}
                          </td>

                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                            {item.from.name}
                          </th>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item.amount}
                          </td>
                          <th className="uppercase border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                            {item.methodShow}
                          </th>
                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                            {item.to?.name ? item.to?.name : "Unknown"}
                          </th>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            {item.transactionDay}
                          </td>
                          <td className="text-center border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                            <i className="fas fa-trash cursor-pointer text-blueGray-500" onClick={(e) => handleDeleteClick(e, item._id)}></i>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                          <p className="p-4 w-12/12 test-center">
                            No records found!
                          </p>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          ) : (
            <></>
          )}
        </>
      )}
    </>
  );
};

Transaction.layout = BoardLayout;

export default Transaction;
