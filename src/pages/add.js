import React, { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import BoardLayout from "../components/Layouts/Board";
import CardEntry from "../components/Molecule/Cards/CardEntry";
import CardDeleteTransaction from "../components/Molecule/Cards/CardDeleteTransaction";
// components
const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)  

const AddPage = () => {
  const deleteCardRef = useRef(null)
  const router = useRouter();
  const { transactionId } = router.query;

  useEffect(() => {
    if (transactionId && transactionId !== "") {
      setTimeout(() => {
        scrollToRef(deleteCardRef)
      }, 300)
    }
  }, [transactionId]);

  return (
    <>
      <nav className=" w-full bg-transparent md:flex-row md:flex-nowrap md:justify-start flex items-center p-4 mb-6">
        <div className="w-full mx-autp items-center flex justify-between md:flex-nowrap flex-wrap">
          <span className="cursor-pointer uppercase text-black text-sm uppercase inline-block font-semibold">
            Add / Delete Entires
          </span>
        </div>
      </nav>
      <div className="flex flex-wrap">
        <div className="w-full xl:w-8/12 px-4">
          <CardEntry />
        </div>
        <div className="w-full xl:w-4/12 px-4" ref={deleteCardRef}>
          <CardDeleteTransaction transactionId={transactionId} />
        </div>
      </div>
    </>
  );
};
AddPage.layout = BoardLayout;

export default AddPage;
