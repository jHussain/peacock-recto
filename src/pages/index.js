import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { getMethod } from "../utils/integration";
import BoardLayout from "../components/Layouts/Board";

import CardTreasury from "../components/Molecule/Cards/CardTreasury";

import { transformClubData } from "../utils/transform";
import Loader from "../components/Molecule/Loader";
import Stats from "../components/Molecule/Stats";

const Home = () => {
  const [clubData, setClubData] = useState({});
  const [isLoading, setLoading] = useState(true);

  const loadData = async () => {
    const { success, data } = await getMethod(`treasury`);
    if (success) {
      setClubData(transformClubData(data));
      setLoading(false);
    } else {
      setLoading(true);
    }
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <>
      <div className="flex flex-wrap justify-center mb-8">
        <Image
          src="/image/peacock.png"
          alt="Peacock Club LOGO"
          className="rounded-full"
          objectFit="cover"
          quality={100}
          height={200}
          width={200}
        />
      </div>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <div className="flex flex-wrap mb-8">
            <div className="w-full xl:w-12/12 px-4">
              {clubData?.statsData && <Stats statsData={clubData.statsData} />}
            </div>
          </div>
        </>
      )}
      <>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            {isLoading && clubData ? (
              <Loader />
            ) : (
              <CardTreasury
                data={clubData.members}
                monthsDif={clubData.monthsDif}
                isLoading={isLoading}
                title="Members treasury"
              />
            )}
          </div>
        </div>
        <div className="flex flex-wrap mb-8">
          <div className="w-full xl:w-12/12">
            {isLoading && clubData ? (
              <Loader />
            ) : (
              <CardTreasury
                data={clubData.vendors}
                monthsDif={clubData.monthsDif}
                isLoading={isLoading}
                isVendor={true}
                title="Vendor treasury"
              />
            )}
          </div>
        </div>

        <div className="flex flex-wrap mb-8">
          <div className="relative flex flex-row justify-center flex-wrap min-w-0 break-words bg-white w-full  shadow-xl rounded-lg px-4 py-8 gap-6">
            {clubData.members ? (
              clubData.members.sort((a, b) => (a?.user?.name > b?.user?.name) ? 1 : -1).map((each) => (
                <>
                  <Link href={`/detail/${each?.user?.value}`} passHref>
                    <div className="flex flex-wrap cursor-pointer flex-col justify-center">
                      <div className="flex justify-center">
                      <Image
                        alt={each?.user?.name}
                        src={`/image/${each?.user?.image}`}
                        className="shadow-lg rounded-full"
                        objectFit="cover"
                        quality={100}
                        height={90}
                        width={90}
                      />
                      </div>
                      <div className="pt-2 text-center">
                        <h5 className="text-xs font-bold">
                          {each?.user?.name.split(" ")[0]}
                        </h5>
                      </div>
                    </div>
                  </Link>
                </>
              ))
            ) : (
              <></>
            )}
          </div>
        </div>
      </>
    </>
  );
};

Home.layout = BoardLayout;

export default Home;
